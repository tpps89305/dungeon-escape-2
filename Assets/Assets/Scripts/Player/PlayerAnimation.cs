﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{

    private Animator _swordAnim;
    private Animator _swordArkAnim;

    // Start is called before the first frame update
    void Start()
    {
        _swordAnim = transform.GetChild(0).GetComponent<Animator>();
        _swordArkAnim = transform.GetChild(1).GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Move(float move)
    {
        _swordAnim.SetFloat("Move", Mathf.Abs(move));
    }

    public void Jump(bool isJumping)
    {
        _swordAnim.SetBool("Jump", isJumping);
    }

    public void Attack()
    {
        _swordAnim.SetTrigger("Attack");
        _swordArkAnim.SetTrigger("Animate");
    }

    public void Death() {
        _swordAnim.SetTrigger("Death");
    }
}
