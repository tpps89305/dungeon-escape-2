﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour, IDamageable
{

    private Rigidbody2D _rigid;
    [SerializeField]
    private float jumpForce = 5.0f;
    private bool resetJump = false;
    [SerializeField]
    private float speed = 3.0f;
    private PlayerAnimation playAnim;
    private SpriteRenderer spritePlayer;
    private SpriteRenderer spriteSwordArk;
    private bool grounded = false;
    public int Diamonds { get; set; }

    public int Health { get; set; }

    private bool isDead = false;

    // Start is called before the first frame update
    void Start()
    {
        _rigid = GetComponent<Rigidbody2D>();
        playAnim = GetComponent<PlayerAnimation>();
        spritePlayer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        spriteSwordArk = transform.GetChild(1).GetComponent<SpriteRenderer>();
        Health = 4;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        if (CrossPlatformInputManager.GetButtonDown("A_Button") && IsGrounded())
        {
            playAnim.Attack();
        }
    }
    // Todo 修正碰撞箱沒有跟著角色一起轉向的問題
    void Movement()
    {
        // float move = Input.GetAxisRaw("Horizontal");
        float move = CrossPlatformInputManager.GetAxis("Horizontal");
        _rigid.velocity = new Vector2(move * speed, _rigid.velocity.y);
        grounded = IsGrounded();

        // Debug.Log("move = " + move);

        if (move > 0)
        {
            spritePlayer.flipX = false;
            spriteSwordArk.flipX = false;
        }
        else if (move < 0)
        {
            spritePlayer.flipX = true;
            spriteSwordArk.flipX = true;
        }

        playAnim.Move(move);
        
        // 注意 & 和 && 的差別
        if ((Input.GetButtonDown("Jump") || CrossPlatformInputManager.GetButtonDown("B_Button")) && grounded)
        {
            Debug.Log("Jump!");
            playAnim.Jump(true);
            _rigid.velocity = new Vector2(_rigid.velocity.x, jumpForce);
            StartCoroutine(ResetJumpRoutine());
        }
        
    }

    bool IsGrounded()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.down, 0.8f, 1 << 8);
        Debug.DrawRay(transform.position, Vector2.down * 0.8f, Color.green);
        //return hitInfo.collider != null;
        if (hitInfo.collider != null)
        {
            if (!resetJump)
            {
                // Debug.Log("Grounded");
                playAnim.Jump(false);
                return true;
            }
        }
        // Debug.Log("Jumping");
        return false;
    }

    IEnumerator ResetJumpRoutine()
    {
        resetJump = true;
        yield return new WaitForSeconds(0.1f);
        resetJump = false;
    }

    public void Damage(int damage)
    {
        if (Health < 1)
        {
            return;
        }
        Health -= damage;
        Debug.Log("玩家受到傷害，剩餘生命值為 " + Health + " 。");
        UIManager.Instance.UpdateLives(Health);
        if (Health < 1)
        {
            playAnim.Death();
            isDead = true;
        }
    }

    public void AddGems(int amount)
    {
        Diamonds += amount;
        UIManager.Instance.UpdateGemCount(Diamonds);
    }
}
