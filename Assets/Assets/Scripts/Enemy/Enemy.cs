﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    [SerializeField]
    public GameObject diamondPrefab;
    [SerializeField]
    protected int health;
    [SerializeField]
    protected int speed;
    [SerializeField]
    protected int gems;
    [SerializeField]
    protected Transform pointA, pointB;

    protected Vector3 currentTarget;
    protected Animator animator;
    protected SpriteRenderer spriteRenderer;

    protected bool isHit = false;
    protected bool isDead = false;

    protected Player player;

    public virtual void Init()
    {
        currentTarget = pointA.position;
        animator = transform.GetChild(0).GetComponent<Animator>();
        spriteRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    private void Start()
    {
        Init();
    }

    private void Update()
    {
        if (isHit)
        {
            ResetByDistance();
        }
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Idle") && animator.GetBool("InCombat") == false)
        {
            return;
        }
        if (!isDead)
        {
            Movement();
        }
    }

    public virtual void Movement()
    {
        // TODO 優化轉向動畫
        if (transform.position == pointA.position)
        {
            spriteRenderer.flipX = true;
        }
        else if (transform.position == pointB.position)
        {
            spriteRenderer.flipX = false;
        }
        if (transform.position == pointA.position)
        {
            currentTarget = pointB.position;
            animator.SetTrigger("Idle");
        }
        else if (transform.position == pointB.position)
        {
            currentTarget = pointA.position;
            animator.SetTrigger("Idle");
        }
        if (!isHit)
        {
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, currentTarget, step);
            // Debug.Log(gameObject.name + "移動");
        }

        Vector3 direction = player.transform.localPosition - transform.localPosition;
        if (direction.x >= 0 && animator.GetBool("InCombat"))
        {
            spriteRenderer.flipX = false;
        }
        else if (direction.x < 0 && animator.GetBool("InCombat"))
        {
            spriteRenderer.flipX = true;
        }
    }

    private void ResetByDistance()
    {
        float distanceWithPlayer = Vector3.Distance(transform.position, player.transform.position);
        if (Mathf.Abs(distanceWithPlayer) > 2f)
        {
            isHit = false;
            animator.SetBool("InCombat", false);
            Debug.Log("重置 isHit 和 InCombat 為 false");
        }
    }

}
