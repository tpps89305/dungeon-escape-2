﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton : Enemy, IDamageable
{

    public int Health { get; set;}

    public void Damage(int damageAmount) {
        animator.SetTrigger("Hit");
        Health -= 10;
        isHit = true;
        animator.SetBool("InCombat", true);
        Debug.Log("Skeleton Health " + Health);
        if (Health < 1)
        {
            animator.SetTrigger("Death");
            isDead = true;
            GameObject diamond = Instantiate(diamondPrefab, transform.position, Quaternion.identity);
            diamond.GetComponent<Diamond>().gems = base.gems;
        }
    }

    public override void Movement()
    {
        base.Movement();
    }

    public override void Init()
    {
        base.Init();
        Health = base.health;
    }
}
