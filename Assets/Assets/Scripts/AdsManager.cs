﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour
{

    public Player player;

    private void Start() {
        // 手動初始化才能順利運行
        Advertisement.Initialize("3715345");
    }

    public void ShowRewardAd()
    {
        //Todo 有時候無法順利執行廣告，但有時卻可以？
        Debug.Log("顯示廣告");
        Advertisement.Initialize("3715345");
        if (Advertisement.IsReady())
        {
            var options = new ShowOptions
            {
                resultCallback = HandleShowResult
            };
            Advertisement.Show("rewardedVideo", options);
        }
        else
        {
            Debug.Log("廣告還沒準備好");
        }
    }

    void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("已觀賞廣告，請笑納獎勵。");
                player.AddGems(100);
                UIManager.Instance.OpenShop(player.Diamonds);
                break;
            case ShowResult.Skipped:
                Debug.Log("跳過了廣告⋯⋯");
                break;
            case ShowResult.Failed:
                Debug.LogError("播放廣告時發生錯誤！");
                break;
        }
    }
}
