﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public GameObject shopUI;
    private int currentSelectIndex = -1;
    private int[] prices = new int[] { 200, 400, 100 };
    private Player player;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            player = other.GetComponent<Player>();
            if (player != null)
            {
                UIManager.Instance.OpenShop(player.Diamonds);
            }
            shopUI.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            shopUI.SetActive(false);
        }
    }

    public void SelectItem(int index)
    {
        Debug.Log("SelectItem(" + index + ")");
        currentSelectIndex = index;
        switch (index)
        {
            case 0:
                UIManager.Instance.UpdateShopSelection(400);
                break;
            case 1:
                UIManager.Instance.UpdateShopSelection(300);
                break;
            case 2:
                UIManager.Instance.UpdateShopSelection(200);
                break;
        }
    }

    public void BuyItem()
    {
        if (player.Diamonds >= prices[currentSelectIndex])
        {
            if (currentSelectIndex == 2)
            {
                GameManager.Instance.HasKeyToCastle = true;
            }
            Debug.Log("購買第 " + currentSelectIndex + " 項商品");
            player.Diamonds -= prices[currentSelectIndex];
            UIManager.Instance.UpdateGemCount(player.Diamonds);
            UIManager.Instance.OpenShop(player.Diamonds);
        }
        else
        {
            Debug.Log("無法購買第 " + currentSelectIndex + " 項商品，所持金錢不足");
        }
    }
}
