﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{

    public bool canDamage = true;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Hit: " + other.name);
        IDamageable hit = other.GetComponent<IDamageable>();

        if (hit != null)
        {
            if (canDamage)
            {
                if (other.tag == "Player")
                {
                    hit.Damage(1);
                }
                else
                {
                    hit.Damage(10);
                }
                StartCoroutine(ResetDamageRoutine());
            }
        }
    }

    IEnumerator ResetDamageRoutine()
    {
        canDamage = false;
        yield return new WaitForSeconds(0.5f);
        canDamage = true;
    }

}
