﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diamond : MonoBehaviour
{

    public int gems = 1;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
            Player player = collision.GetComponent<Player>();
            if (player != null) {
                player.AddGems(gems);
                Debug.Log("已撿取寶石，現在持有 " + player.Diamonds + " 個寶石。");
            }
        }
    }
}
