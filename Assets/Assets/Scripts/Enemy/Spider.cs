﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : Enemy, IDamageable
{
    public GameObject acidEffectPrefab;

    public int Health { get; set;}

    public override void Init()
    {
        base.Init();
        Health = base.health;
        print("蜘蛛的生命有 " + Health + " 點");
    }

    public void Damage(int damageAmount) {
        print("蜘蛛受到 " + damageAmount + " 點偒害");
        Health -= damageAmount;
        if (Health < 1)
        {
            animator.SetTrigger("Death");
            isDead = true;
            GameObject diamond = Instantiate(diamondPrefab, transform.position, Quaternion.identity);
            diamond.GetComponent<Diamond>().gems = base.gems;
        }
    }

    public override void Movement()
    {
        // 這個角色不會移動
    }

    public void Attack()
    {
        Instantiate(acidEffectPrefab, transform.position, Quaternion.identity);
    }
}
