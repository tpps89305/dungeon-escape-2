﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidEffect : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // 讓這個物件在 5 秒後消失
        Destroy(gameObject, 5.0f);
    }

    // Update is called once per frame
    void Update()
    {
        // 使物件持續往一個方向移動
        transform.Translate(Vector3.right * 3 * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            IDamageable hit = collision.GetComponent<IDamageable>();
            if (hit != null)
            {
                hit.Damage(1);
                Destroy(gameObject);
            }
        }
    }

}
