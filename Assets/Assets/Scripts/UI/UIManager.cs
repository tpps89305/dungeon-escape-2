﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager _instance;
    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("UIManager 尚未初始化");
            }
            return _instance;
        }
    }

    public Text playerGemCount;
    public Image selectionSlider;
    public Text gemsCountText;
    public Image[] healthBars;

    public void OpenShop(int gemCount) {
        playerGemCount.text = gemCount.ToString() + " G";
    }

    public void UpdateShopSelection(int yPos) {
        selectionSlider.rectTransform.anchoredPosition = new Vector2(selectionSlider.rectTransform.anchoredPosition.x, yPos);
    }

    public void UpdateGemCount(int count)
    {
        gemsCountText.text = count.ToString();
    }

    public void UpdateLives(int liveRemaining)
    {
        for (int i = 0; i <= liveRemaining; i++)
        {
            if (i == liveRemaining)
            {
                healthBars[i].enabled = false;
            }
        }
    }

    private void Awake() {
        _instance = this;
    }
}
